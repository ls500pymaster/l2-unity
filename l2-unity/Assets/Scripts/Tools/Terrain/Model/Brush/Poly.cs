#if (UNITY_EDITOR) 
[System.Serializable]
public class Poly {
    public string name;
    public int polyCount;
    public PolyData[] polyData;
}
#endif